﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_Arithmetic_Calculator
{
    public partial class Form1 : Form
    {
        int FirstNumber, SecondNumber, sum, diff, multip, div;

        private void Division_Click(object sender, EventArgs e)
        {
            //Division
            FirstNumber = int.Parse(textBox1.Text);
            SecondNumber = int.Parse(textBox2.Text);

            div = FirstNumber / SecondNumber;
            MessageBox.Show("The division of the two numbers is " + div.ToString());
        }

        private void Multiplication_Click(object sender, EventArgs e)
        {
            //Multiplication
            FirstNumber = int.Parse(textBox1.Text);
            SecondNumber = int.Parse(textBox2.Text);

            multip = FirstNumber * SecondNumber;
            MessageBox.Show("The multiplication of the two numbers is " + multip.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Subtract
            FirstNumber = int.Parse(textBox1.Text);
            SecondNumber = int.Parse(textBox2.Text);

            diff = FirstNumber - SecondNumber;
            MessageBox.Show("The difference between the two numbers is " + diff.ToString());
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Add
            FirstNumber = int.Parse(textBox1.Text);
            SecondNumber = int.Parse(textBox2.Text);

            sum = FirstNumber + SecondNumber;
            MessageBox.Show("The sum of the two numbers is " + sum.ToString());
        }
    }
}
